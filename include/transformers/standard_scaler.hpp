#ifndef STANDARD_SCALER_HPP
#define STANDARD_SCALER_HPP

#include "transformers.hpp"

namespace cpp_ml::models::transformers {

class standard_scaler : public data_transformer {
	private:
		// a mean and standard_deviation per data column
		Eigen::RowVectorXd means_;
		Eigen::RowVectorXd standards_deviations_;

	public:

		standard_scaler();
		virtual ~standard_scaler();

		virtual auto transform(const Eigen::MatrixXd & data) const -> Eigen::MatrixXd override;

		virtual auto fit(const Eigen::MatrixXd & data) -> void override;

		auto get_means() const -> Eigen::RowVectorXd;

		auto get_standard_deviations() const -> Eigen::RowVectorXd;

};


} // end namespace models::transformers

#endif
