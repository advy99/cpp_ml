#ifndef REGRESSORS_HPP
#define REGRESSORS_HPP

#include <vector>
#include <cstdint>

#include <Eigen/Dense>

namespace cpp_ml::models::regressors {

class regressor {
	private:

	public:
		virtual ~regressor () { }

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> double = 0;

		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd = 0;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void = 0;


};

} // end models::regressors namespace



#endif
