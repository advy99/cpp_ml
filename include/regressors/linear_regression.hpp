#ifndef LINEAR_REGRESSION_HPP
#define LINEAR_REGRESSION_HPP

#include "regressors.hpp"
#include <functional>

namespace cpp_ml::models::regressors {

class linear_regression : public regressor {
	private:
		Eigen::VectorXd weights_ {};

	public:

		virtual ~linear_regression();

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> double override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void override;

};


} // end of models::regressors namespace

#endif
