#ifndef POLYNOMIAL_REGRESSION_HPP
#define POLYNOMIAL_REGRESSION_HPP

#include "regressors.hpp"
#include <functional>
#include <cmath>

namespace cpp_ml::models::regressors {

class polynomial_regression : public regressor {
	private:
		std::size_t degree_;
		Eigen::VectorXd weights_ {};


		auto convert_instance_to_polynomial(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd;

	public:

		virtual ~polynomial_regression();

		polynomial_regression();
		polynomial_regression(const std::size_t degree);

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> double override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void override;

};


} // end of models::regressors namespace

#endif
