#ifndef KNNREGRESSOR_HPP
#define KNNREGRESSOR_HPP

#include "regressors.hpp"
#include <functional>

namespace cpp_ml::models::regressors {

class knn_regressor : public regressor {
	private:
		Eigen::MatrixXd data_ {};
		Eigen::VectorXd targets_ {};
		std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> distance_function_;
		std::size_t k_;

	public:

		knn_regressor(std::size_t k, const std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> & distance_f);
		virtual ~knn_regressor();

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> double override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void override;

};


} // end of models::regressors namespace

#endif
