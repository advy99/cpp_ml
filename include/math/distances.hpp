#ifndef DISTANCES_HPP
#define DISTANCES_HPP

#include <vector>
#include <cstdint>
#include <functional>

#include <Eigen/Dense>

namespace cpp_ml::math::distances {

auto minkowski_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b, int32_t p) -> double;

auto euclidean_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b) -> double;

auto manhattan_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b) -> double;

auto compute_k_nearest_neighbors(const Eigen::RowVectorXd & instance,
											const Eigen::MatrixXd & data,
											const std::size_t k,
											std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> distance_function
) -> std::vector<std::size_t>;

} // end of math::distance namespace

#endif
