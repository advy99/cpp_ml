#ifndef CLASSIFICATION_METRICS
#define CLASSIFICATION_METRICS

#include <vector>
#include <cstdint>

#include <Eigen/Dense>

namespace cpp_ml::metrics::classification_metrics {

auto accuracy_score (const Eigen::VectorXi & y_true, const Eigen::VectorXi & y_pred) -> double;

} // end metrics::classification_metrics namespace



#endif
