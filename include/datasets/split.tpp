#include <Eigen/src/Core/util/Constants.h>
#include <tuple>
#include <random>
#include <numeric>
#include <algorithm>

#include <Eigen/Dense>

namespace cpp_ml::datasets::split {

template <typename T>
auto train_test_split (
	const Eigen::MatrixXd & data,
	const Eigen::Matrix<T, Eigen::Dynamic, 1> & targets,
	const double test_size,
	const uint64_t seed
) -> std::tuple<Eigen::MatrixXd, Eigen::Matrix<T, Eigen::Dynamic, 1>,
	  			Eigen::MatrixXd, Eigen::Matrix<T, Eigen::Dynamic, 1>> {

	size_t test_rows = static_cast<size_t>(static_cast<double>(data.rows()) * test_size);

	Eigen::MatrixXd x_train = Eigen::MatrixXd::Zero( data.rows() - test_rows, data.cols() );
	Eigen::MatrixXd x_test = Eigen::MatrixXd::Zero( test_rows, data.cols() );

	Eigen::Matrix<T, Eigen::Dynamic, 1> y_train = Eigen::Matrix<T, Eigen::Dynamic, 1>::Zero( data.rows() - test_rows, 1);
	Eigen::Matrix<T, Eigen::Dynamic, 1> y_test = Eigen::Matrix<T, Eigen::Dynamic, 1>::Zero(test_rows, 1);

	std::vector<std::size_t> index (data.rows());
	std::iota(index.begin(), index.end(), 0);

	std::mt19937 random_generator;
	random_generator.seed(seed);

	std::shuffle(index.begin(), index.end(), random_generator);

	std::size_t i = 0;

	Eigen::Index test_idx = 0;
	while (i < test_rows) {
		x_test.row(test_idx) = data.row(index[i]);
		y_test.row(test_idx) = targets.row(index[i]);
		++i;
		++test_idx;
	}


	Eigen::Index train_idx = 0;
	while (i < static_cast<std::size_t>(data.rows()) ) {
		x_train.row(train_idx) = data.row(index[i]);
		y_train.row(train_idx) = targets.row(index[i]);
		++i;
		++train_idx;
	}

	return std::make_tuple(x_train, y_train, x_test, y_test);

}


} //end datasets::reading namespace
