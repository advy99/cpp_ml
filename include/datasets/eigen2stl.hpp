#include <Eigen/Dense>

namespace cpp_ml::datasets::eigen2stl {

auto vectors2EigenMatrix(const std::vector<std::vector<double>> & vec) -> Eigen::MatrixXd;

auto vector2EigenVector(const std::vector<double> & vec) -> Eigen::VectorXd;

auto vector2EigenVector(const std::vector<std::vector<int>> & vec) -> Eigen::VectorXi;

}
