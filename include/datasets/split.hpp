#ifndef SPLIT_HPP
#define SPLIT_HPP

#include <string>
#include <vector>

#include <Eigen/Dense>

namespace cpp_ml::datasets::split {

template <typename T>
auto train_test_split (
	const Eigen::MatrixXd & data,
	// simulate VectorX<T>
	const Eigen::Matrix<T, Eigen::Dynamic, 1> & targets,
	const double test_size = 0.8,
	const uint64_t seed = 42
) -> std::tuple<Eigen::MatrixXd, Eigen::Matrix<T, Eigen::Dynamic, 1>,
	  			Eigen::MatrixXd, Eigen::Matrix<T, Eigen::Dynamic, 1>>;

} //end datasets::reading namespace


#include "datasets/split.tpp"


#endif
