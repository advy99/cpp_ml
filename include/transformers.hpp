#ifndef TRANSFORMERS_HPP
#define TRANSFORMERS_HPP

#include <vector>
#include <string>
#include <cstdint>

#include <Eigen/Dense>

namespace cpp_ml::models::transformers {

class target_transformer {
	private:

	public:
		virtual ~target_transformer() { }

		virtual auto transform(const std::vector<std::string> & instance) const -> Eigen::VectorXi = 0;

		virtual auto fit(const std::vector<std::string> & targets) -> void = 0;


};


class data_transformer {
	private:

	public:
		virtual ~data_transformer() { }

		virtual auto transform(const Eigen::MatrixXd & data) const -> Eigen::MatrixXd = 0;

		virtual auto fit(const Eigen::MatrixXd & data) -> void = 0;


};


} // end models::transformers namespace



#endif
