#ifndef KNNCLASSIFIER_HPP
#define KNNCLASSIFIER_HPP

#include "classifiers.hpp"
#include <functional>

namespace cpp_ml::models::classifiers {

class knn_classifier : public classifier {
	private:
		Eigen::MatrixXd data_ {};
		Eigen::VectorXi targets_ {};
		std::function<double(const Eigen::RowVectorXd &, const Eigen::VectorXd &)> distance_function_;
		size_t k_ {};

	public:

		knn_classifier(size_t k, const std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> & distance_f);
		virtual ~knn_classifier();

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> int override;
		virtual auto predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi override;
		virtual auto predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void override;

};


} // end of classifiers namespace

#endif
