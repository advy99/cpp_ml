namespace cpp_ml::models::classifiers {

template <class T>
requires std::derived_from<T, classifier>
one_versus_all_classifier<T> :: one_versus_all_classifier (const T & estimator)
	:base_estimator_ (estimator)
{}

template <class T>
requires std::derived_from<T, classifier>
one_versus_all_classifier<T> :: ~one_versus_all_classifier() {}

template <class T>
requires std::derived_from<T, classifier>
auto one_versus_all_classifier<T> :: predict(const Eigen::RowVectorXd & instance) const -> int {

	Eigen::RowVectorXd probabilities = predict_probabilities(instance);

	Eigen::Index max_coeff_index = 0;

	probabilities.maxCoeff(&max_coeff_index);

	int prediction = static_cast<int>(max_coeff_index);

	return prediction;

}


template <class T>
requires std::derived_from<T, classifier>
auto one_versus_all_classifier<T> :: predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd {
	
	Eigen::RowVectorXd probabilities = Eigen::RowVectorXd::Zero(num_classes_);

	for (std::size_t i = 0; i < estimators_.size(); ++i) {
		// save the probability of beeing in the class i
		probabilities(i) = estimators_[i].predict_probabilities(instance)[1];
	}

	// TODO: Apply min_max_scaler to probabilities

	return probabilities;
}



template <class T>
requires std::derived_from<T, classifier>
auto one_versus_all_classifier<T> :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi {

	Eigen::VectorXi predictions = Eigen::VectorXi::Zero(new_data.rows());

	for (Eigen::Index i = 0; const Eigen::RowVectorXd & row : new_data.rowwise()) {
		predictions(i) = predict(row);
		++i;
	}

	return predictions;

}

template <class T>
requires std::derived_from<T, classifier>
auto one_versus_all_classifier<T> :: fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void {

	this->classifier::fit(data, targets);

	estimators_.clear();
	estimators_.resize(num_classes_);

	for (std::size_t i = 0; i < num_classes_; ++i) {
		Eigen::VectorXi new_targets = Eigen::VectorXi::Zero(targets.rows());

		for (Eigen::Index j = 0; const int target : targets) {
			int value;
			if (target == static_cast<int>(i)) {
				value = 1;
			} else {
				value = 0;
			}
			new_targets(j) = value;
			++j;
		}

		estimators_[i] = base_estimator_;
		estimators_[i].fit(data, new_targets);

	}

}

template <class T>
requires std::derived_from<T, classifier>
auto one_versus_all_classifier<T> :: predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd {
	Eigen::MatrixXd predictions = Eigen::MatrixXd::Zero(new_data.rows(), num_classes_);


	for (Eigen::Index i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions.row(i) = predict_probabilities(data);
	}

	return predictions;


}



} // end cpp_ml::models::classifiers
