#ifndef ONE_VERSUS_ALL_CLASSIFIER_HPP
#define ONE_VERSUS_ALL_CLASSIFIER_HPP

#include <concepts>
#include "classifiers.hpp"

namespace cpp_ml::models::classifiers {

template <class T>
requires std::derived_from<T, classifier>
class one_versus_all_classifier : public classifier {
	private:
		T base_estimator_;
		std::vector<T> estimators_ {};

	public:

		one_versus_all_classifier(const T & estimator);
		virtual ~one_versus_all_classifier();

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> int override;
		virtual auto predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi override;
		virtual auto predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void override;


};


} // end cpp_ml::models::classifiers

#include "classifiers/multiclass/one_versus_all_classifier.tpp"

#endif
