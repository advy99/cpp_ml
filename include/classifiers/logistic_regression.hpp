#ifndef LOGISTIC_REGRESSION_HPP
#define LOGISTIC_REGRESSION_HPP

#include "classifiers.hpp"
#include <functional>

namespace cpp_ml::models::classifiers {

class logistic_regression : public classifier {
	private:
		Eigen::VectorXd weights_ {};
		double learning_rate_;
		int max_iters_;
		std::uint64_t random_seed_;


		auto compute_logistic_function(const Eigen::RowVectorXd & instance) const -> double;

	public:

		logistic_regression (const double learning_rate = 0.1, const int max_iter = 1000, const std::uint64_t seed = 0);
		~logistic_regression() = default;

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> int override;
		virtual auto predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd override;
		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi override;
		virtual auto predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd override;

		virtual auto fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void override;

};


} // end of models::regressors namespace

#endif
