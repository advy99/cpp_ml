#ifndef CLASSIFIER_HPP
#define CLASSIFIER_HPP

#include <vector>
#include <cstdint>
#include <set>

#include <Eigen/Dense>

namespace cpp_ml::models::classifiers {

class classifier {
	protected:
		std::size_t num_classes_ {};

	public:
		virtual ~classifier() = default; 

		virtual auto predict(const Eigen::RowVectorXd & instance) const -> int = 0;

		virtual auto predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd = 0;

		virtual auto predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi = 0;
		virtual auto predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd = 0;

		virtual auto fit( [[maybe_unused]] const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void {
			num_classes_ = std::set(targets.begin(), targets.end()).size();
		}


};

} // end models::classifiers namespace



#endif
