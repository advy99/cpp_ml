#include "metrics/regression_metrics.hpp"

#include <cmath>
#include <stdexcept>

namespace cpp_ml::metrics::regression_metrics {

auto mean_absolute_error (const Eigen::VectorXd & y_true, const Eigen::VectorXd & y_pred) -> double {

	if (y_true.size() != y_pred.size()) {
		throw std::range_error("y_true and y_pred have inconsistent size.");
	}

	double mae = ((y_true - y_pred).cwiseAbs()).mean();

	return mae;

}

auto mean_squared_error (const Eigen::VectorXd & y_true, const Eigen::VectorXd & y_pred) -> double {


	if (y_true.size() != y_pred.size()) {
		throw std::range_error("y_true and y_pred have inconsistent size.");
	}

	double mse = (y_true - y_pred).array().pow(2).mean();

	return mse;

}

auto root_mean_squared_error (const Eigen::VectorXd & y_true, const Eigen::VectorXd & y_pred) -> double {
	return std::sqrt( mean_squared_error(y_true, y_pred) );
}


} // end metrics::regression_metrics namespace
