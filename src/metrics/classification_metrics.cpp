#include "metrics/classification_metrics.hpp"
#include <stdexcept>

namespace cpp_ml::metrics::classification_metrics {

auto accuracy_score (const Eigen::VectorXi & y_true, const Eigen::VectorXi & y_pred) -> double {

	if (y_true.size() != y_pred.size()) {
		throw std::range_error("y_true and y_pred have inconsistent size.");
	}

	int num_corrects = 0;

	// count corrects predictions
	for (Eigen::Index i = 0; i < y_true.rows(); ++i) {
		if (y_true(i) == y_pred(i)) {
			num_corrects += 1;
		}
	}

	// divide by total number of instances
	return (static_cast<double>(num_corrects) / static_cast<double>(y_true.rows()));

}



} // end metrics::classification_metrics namespace
