#include "datasets/eigen2stl.hpp"


namespace cpp_ml::datasets::eigen2stl {

auto vectors2EigenMatrix(const std::vector<std::vector<double>> & vec) -> Eigen::MatrixXd {
	Eigen::MatrixXd result = Eigen::MatrixXd::Zero(vec.size(), vec.at(0).size());


	for(std::size_t i = 0; i < vec.size(); ++i) {
		for(std::size_t j = 0; j < vec[i].size(); ++j) {
			result(i, j) = vec[i][j];
		}
	}

	return result;
}

auto vector2EigenVector(const std::vector<double> & vec) -> Eigen::VectorXd {
	Eigen::VectorXd result = Eigen::VectorXd::Zero(vec.size());

	for(std::size_t i = 0; i < vec.size(); ++i) {
		result(i) = vec[i];
	}

	return result;

}

auto vector2EigenVector(const std::vector<int> & vec) -> Eigen::VectorXi {

	Eigen::VectorXi result = Eigen::VectorXi::Zero(vec.size());

	for(std::size_t i = 0; i < vec.size(); ++i) {
		result(i) = vec[i];
	}

	return result;
}

} // end namespace datasets::eigen2stl

