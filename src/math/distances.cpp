#include <math/distances.hpp>

#include <cmath>
#include <queue>
#include <functional>

namespace cpp_ml::math::distances {

auto minkowski_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b, int32_t p) -> double {
	// minkowski distance as in the formula
	double distance = 0.0;

	distance = (point_a - point_b).cwiseAbs().array().pow(p).sum();

	distance = std::pow(distance, 1.0 / p);

	return distance;
}

auto euclidean_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b) -> double {
	// euclidean distance is the minkowski distance with p = 2
	return minkowski_distance(point_a, point_b, 2);
}

auto manhattan_distance(const Eigen::RowVectorXd & point_a, const Eigen::RowVectorXd & point_b) -> double {
	// euclidean distance is the minkowski distance with p = 1
	return minkowski_distance(point_a, point_b, 1);
}



auto compute_k_nearest_neighbors(const Eigen::RowVectorXd & instance,
											const Eigen::MatrixXd & data,
											const std::size_t k,
											std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> distance_function
) -> std::vector<std::size_t> {
	
	// a vector for the neighbors and a priority_queue with the distances
	std::vector<std::size_t> neighbors;
	// we use std::greater, so closest distances are in top.
	std::priority_queue<std::pair<double, std::size_t>,
							  std::vector<std::pair<double,
							  std::size_t>>, std::greater<std::pair<double, std::size_t>>> distances;

	// compute the distance of the instance with every data point
	for (Eigen::Index i = 0; i < data.rows(); i++ ) {
		distances.push(std::make_pair(distance_function(instance, data.row(i)), i) );
	}

	neighbors.resize(k);

	// select the k closer's neighbors
	for (std::size_t i = 0; i < neighbors.size(); i++ ) {
		neighbors[i] = distances.top().second;
		distances.pop();
	}

	return neighbors;

}


}
