#include "regressors/knn_regressor.hpp"
#include "math/distances.hpp"

#include <vector>
#include <map>

namespace cpp_ml::models::regressors {

knn_regressor :: knn_regressor(std::size_t k, const std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> & distance_f)
:
	distance_function_ { distance_f },
	k_ { k }
{ }

knn_regressor :: ~knn_regressor() {}

auto knn_regressor :: fit(const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void {
	data_ = data;
	targets_ = targets;
}

auto knn_regressor :: predict(const Eigen::RowVectorXd & instance) const -> double {

	// compute the k closer's neighbors
	auto neighbors = math::distances::compute_k_nearest_neighbors(instance, data_, k_, distance_function_);

	// compute the prediction as the mean of the neighbors values
	double prediction = 0.0;

	for (const auto & index : neighbors) {
		prediction += targets_[index];
	}

	prediction = prediction / static_cast<double>(neighbors.size());

	return prediction;


}

auto knn_regressor :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd  {
	Eigen::VectorXd predictions = Eigen::VectorXd::Zero(new_data.rows());


	for (Eigen::Index i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions(i) = predict(data);
		i++;
	}

	return predictions;

}



} // ends models::regressors namespace
