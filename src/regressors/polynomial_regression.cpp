#include "regressors/polynomial_regression.hpp"

namespace cpp_ml::models::regressors {

polynomial_regression :: polynomial_regression ()
	:degree_(1)
{}

polynomial_regression :: polynomial_regression (const std::size_t degree)
	:degree_(degree)
{}

polynomial_regression :: ~polynomial_regression() {}

auto polynomial_regression :: predict(const Eigen::RowVectorXd & instance) const -> double {

	double prediction = weights_(0);

	auto polynomial_instance = convert_instance_to_polynomial(instance);

	for (Eigen::Index i = 1; i < weights_.rows(); ++i) {
		prediction += weights_(i) * polynomial_instance(i - 1);
	}

	return prediction;

}

auto polynomial_regression :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd  {
	Eigen::VectorXd predictions = Eigen::VectorXd::Zero(new_data.rows());

	for (Eigen::Index i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions(i) = predict(data);
		i++;
	}

	return predictions;

}

auto polynomial_regression :: fit (const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void {

	// first, add the variables of the data but powered
	Eigen::MatrixXd polynomial_data = Eigen::MatrixXd::Zero(data.rows(), data.cols() * degree_);

	for (Eigen::Index i = 0; const Eigen::RowVectorXd & row : data.rowwise()) {
		polynomial_data.row(i) = convert_instance_to_polynomial(row);
		++i;
	}

	Eigen::MatrixXd polynomial_data_with_dummy = Eigen::MatrixXd::Ones(polynomial_data.rows(), polynomial_data.cols() + 1);

	polynomial_data_with_dummy.block(0, 1, polynomial_data.rows(), polynomial_data.cols()) = polynomial_data;

	Eigen::MatrixXd data_with_dummie_pseudoinverse = polynomial_data_with_dummy.completeOrthogonalDecomposition().pseudoInverse();

	weights_ = data_with_dummie_pseudoinverse * targets;

}

auto polynomial_regression :: convert_instance_to_polynomial(
		const Eigen::RowVectorXd & instance
) const -> Eigen::RowVectorXd {

	// the new row size is 
	// instance.cols * degree_ (base linear times per degree) 
	Eigen::RowVectorXd result = Eigen::RowVectorXd( instance.cols() * degree_);
	result.block(0, 0, 1, instance.cols()) = instance;

	// for every column
	for (std::size_t i = instance.cols(); i < instance.cols()*degree_; ++i){
		// the index we need is the modulo of  the current column with the original instance columns 
		// the power is the index divided (INTEGER DIVISION!!!) by the total columns
		result(i) = std::pow( instance(i % instance.cols()), static_cast<int>(i / instance.cols()) + 1 );
	}

	return result;

}


} // end namespace models::regressors
