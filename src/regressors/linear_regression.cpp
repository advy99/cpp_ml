#include "regressors/linear_regression.hpp"

namespace cpp_ml::models::regressors {

linear_regression :: ~linear_regression() {}

auto linear_regression :: predict(const Eigen::RowVectorXd & instance) const -> double {

	double prediction = weights_[0];

	for (Eigen::Index i = 1; i < weights_.rows(); ++i) {
		prediction += weights_(i) * instance(i - 1);
	}

	return prediction;

}

auto linear_regression :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXd  {
	Eigen::VectorXd predictions = Eigen::VectorXd::Zero(new_data.rows());


	for (Eigen::Index i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions(i) = predict(data);
		i++;
	}

	return predictions;

}

auto linear_regression :: fit (const Eigen::MatrixXd & data, const Eigen::VectorXd & targets) -> void {

	Eigen::MatrixXd data_with_dummie = Eigen::MatrixXd::Ones(data.rows(), data.cols() + 1);

	// copy all the data, but starting at column 1
	data_with_dummie.block(0, 1, data.rows(), data.cols()) = data;

	// compute the pseudo inverse of the data matrix
	Eigen::MatrixXd data_with_dummie_pseudoinverse = data_with_dummie.completeOrthogonalDecomposition().pseudoInverse();

	// replace the weights with the ones computed from the pseudoInverse of the target
	weights_ = data_with_dummie_pseudoinverse * targets;

}



} // end namespace models::regressors
