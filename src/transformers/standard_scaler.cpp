#include "transformers/standard_scaler.hpp"
#include "math/utils.hpp"

#include <cmath>

#include <iostream>

namespace cpp_ml::models::transformers {

standard_scaler :: standard_scaler ()
{}

standard_scaler :: ~standard_scaler() {}

auto standard_scaler :: transform (const Eigen::MatrixXd & data) const -> Eigen::MatrixXd {

	// we could make a copy in the method argument, but to hold consistency
	// with the data_transformer class
	Eigen::MatrixXd result = (data.rowwise() - means_).array().rowwise() / standards_deviations_.array();

	return result;

}


auto standard_scaler :: fit (const Eigen::MatrixXd & data) -> void {

	// a mean per num of columns, and start with 0.0
	means_ = data.colwise().mean(); // Eigen::RowVectorXd::Zero(data.cols());
	standards_deviations_ = Eigen::RowVectorXd::Zero(data.cols());


	// compute the standard deviation for each column, using the mean we alredy computed
	for (const auto & row : data.rowwise()) {
		for (Eigen::Index i = 0; i < row.cols(); ++i) {
			standards_deviations_(i) += std::pow(row(i) - means_(i), 2);
		}
	}



	for (double & column_std_dev : standards_deviations_) {
		column_std_dev = column_std_dev * (1.0 / static_cast<double>(data.rows()) );
		column_std_dev = std::sqrt(column_std_dev);

		if (column_std_dev == 0.0) {
			column_std_dev = 1.0;
		}
	}


}


auto standard_scaler :: get_means() const -> Eigen::RowVectorXd {
	return means_;
}

auto standard_scaler :: get_standard_deviations() const -> Eigen::RowVectorXd {
	return standards_deviations_;
}



} // ends models::transformers namespace
