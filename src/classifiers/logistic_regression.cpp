#include "classifiers/logistic_regression.hpp"
#include "math/utils.hpp"
#include "random.hpp"

namespace cpp_ml::models::classifiers {

logistic_regression :: logistic_regression (const double learning_rate, const int max_iters, const uint64_t seed)
	:learning_rate_ (learning_rate), max_iters_ (max_iters), random_seed_ (seed)
{}


auto logistic_regression :: predict(const Eigen::RowVectorXd & instance) const -> int {
	// TODO: Modify logistic regression to multiclass problems, not only binary problems ( One VS All ? )

	int prediction = 0;

	double logistic_value = compute_logistic_function(instance);

	if (logistic_value >= 0.5) {
		prediction = 1;
	}

	return prediction;
}

auto logistic_regression :: predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd {


	Eigen::VectorXd probabilities = Eigen::VectorXd::Zero(2);

	double logistic_value = compute_logistic_function(instance);

	probabilities(0) = 1.0 - logistic_value;
	probabilities(1) = logistic_value;

	return probabilities;
}

auto logistic_regression :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi {

	Eigen::VectorXi predictions = Eigen::VectorXi::Zero(new_data.rows());

	for (std::size_t i = 0; const Eigen::RowVectorXd & row : new_data.rowwise()) {
		predictions(i) = predict(row);
		i++;
	}

	return predictions;
}

auto logistic_regression :: fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void {

	this->classifier::fit(data, targets);

	// a weight per column plus one for the bias
	weights_ = Eigen::VectorXd::Zero(data.cols() + 1);

	// start with random weights
	Random::set_seed(random_seed_);

	for (double & weight : weights_) {
		weight = Random::next_double(-50.0, 50.0);
	}

	for (int i = 0; i < max_iters_; ++i) {

		for (Eigen::Index j = 0; j < data.rows(); ++j) {

			double logistic_value = compute_logistic_function(data.row(j));

			double prediction_difference = targets(j) - logistic_value;

			// compute the bias as if the column value is 1.0
			weights_[0] = weights_[0] + learning_rate_ * prediction_difference * logistic_value * (1 - logistic_value); // * 1.0; Bias weight
			for (Eigen::Index k = 1; k < weights_.rows(); ++k) {
				double change = learning_rate_ * prediction_difference * logistic_value * (1 - logistic_value) * data(j, k - 1);

				weights_[k] = weights_[k] + change;
			}

		}

	}

}

auto logistic_regression :: predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd {

	Eigen::MatrixXd probabilities = Eigen::MatrixXd::Zero(new_data.rows(), 2); 

	for (Eigen::Index i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		probabilities.row(i) = predict_probabilities(data);
		i++;
	}

	return probabilities;


}

auto logistic_regression :: compute_logistic_function(const Eigen::RowVectorXd & instance) const -> double {
	// start with the bias (has no corresponding column in an instance)
	double regression_value = weights_[0];

	for (Eigen::Index i = 1; i < weights_.rows(); ++i) {
		regression_value += weights_[i] * instance[i - 1];
	}

	double logistic_value = cpp_ml::math::utils::logistic_function(regression_value);

	return logistic_value;

}


}	// end cpp_ml::models::classifiers namespace
