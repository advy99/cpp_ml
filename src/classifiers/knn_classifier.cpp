#include "classifiers/knn_classifier.hpp"
#include "math/distances.hpp"

#include <vector>
#include <map>
#include <set>

namespace cpp_ml::models::classifiers {

knn_classifier :: knn_classifier(size_t k, const std::function<double(const Eigen::RowVectorXd &, const Eigen::RowVectorXd &)> & distance_f)
:
	distance_function_ { distance_f },
	k_ { k }
{ }

knn_classifier :: ~knn_classifier() {}

auto knn_classifier :: fit(const Eigen::MatrixXd & data, const Eigen::VectorXi & targets) -> void {
	this->classifier::fit(data, targets);
	data_ = data;
	targets_ = targets;
}


auto knn_classifier :: predict_probabilities(const Eigen::RowVectorXd & instance) const -> Eigen::RowVectorXd {

	Eigen::VectorXd probabilities = Eigen::VectorXd::Zero(num_classes_);

	int predicted_class = predict(instance);
	probabilities(static_cast<std::size_t>(predicted_class)) = 1.0;

	return probabilities;
}

auto knn_classifier :: predict(const Eigen::RowVectorXd & instance) const -> int {

	// compute the k closer's neighbors
	auto neighbors = math::distances::compute_k_nearest_neighbors(instance, data_, k_, distance_function_);

	std::map<int, int> count;

	// count the class of the neighbors
	for (auto neighbor: neighbors) {
		if (count.contains(targets_[neighbor])) {
			count[targets_[neighbor]] = count[targets_[neighbor]] + 1;
		} else {
			count[targets_[neighbor]] = 1;
		}
	}

	// the predicted class is the class of the majority closer's points
	auto predicted_class = std::max_element(count.begin(), count.end(),
								[](const std::pair<int, int>& p1, const std::pair<int, int>& p2) {
									return p1.second < p2.second; });


	return predicted_class->first;


}

auto knn_classifier :: predict_probabilities(const Eigen::MatrixXd & new_data) const -> Eigen::MatrixXd {

	Eigen::MatrixXd predictions = Eigen::MatrixXd::Zero(new_data.rows(), num_classes_);

	for ( std::size_t i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions.row(i) = predict_probabilities(data);
		i++;
	}

	return predictions;


}

auto knn_classifier :: predict(const Eigen::MatrixXd & new_data) const -> Eigen::VectorXi  {

	Eigen::VectorXi predictions = Eigen::VectorXi::Zero(new_data.rows());

	for (std::size_t i = 0; const Eigen::RowVectorXd & data : new_data.rowwise() ) {
		predictions(i) = predict(data);
		i++;
	}

	return predictions;

}



} // ends models::classifiers namespace
